The power and energy demands of high-performance
computing (HPC) centers are growing due to an
increasing number of systems on site, an increase in
individual system size \sout{caused by end of Dennard's
scaling}, not allowing for constant power disipation
with increasing performance per chip area
anymore\cite{Dennard,DennardsEnd}, and an increase in
both the rate of change and magnitude of system power
fluctuations.  We have already achieved major gains in
energy-efficiency for both the datacenter and HPC
equipment.  For example, the PUE (Power Usage Efficiency\cite{PUE}) of the Swiss
Supercomputer (CSCS) datacenter prior to 2012 was 1.8,
but the current PUE is about 1.25; a factor of
\texttildelow1.5 improvement. HPC system improvements
have also been very strong, as evidenced by FLOPS/Watt
performance on the Green500 List.  While we have seen
gains from data center and HPC system efficiency, there
are also energy-efficiency gains to be had from
software.  The software stack can orchestrate the
system load, operating point, and user satisfaction to
offer many opportunities for improving energy
efficiency.  There is a lot of research that explores
these software opportunities, but little has been
deployed in a production environment.

Energy and power aware job scheduling and resource
management (EPA JSRM) is identified as a software
capability that is ahead of other software capabilities
in that it actually has been/is being deployed on
large-scale production supercomputers.  Other
capabilities were considered to be interesting, but as
research or investigative technologies.  With these
factors in mind, we investigate how HPC centers are
using energy and power aware job scheduling and
resource management capabilities.  This research
identifies and surveys major HPC sites that have either
actively deployed or are engaged in technology
development with the intention to deploy large-scale
\EPAJSRM technologies in a production environment.  

This survey is designed to cover a broader population
rather than a representative sample of sites. It is a
collection of leading edge implementers or EPA JSRM
capabilities. The survey spans from motivation to
implementation, looking at system components impacted,
user interaction and level of automation of the
solution.  The questions try to be comprehensive in
nature and span a wide range of information.  There is
a wealth of information that was gathered during the
interview process.  Over 100 pages of written and oral
transcripts interview data was generated and is
available to the HPC community.  The survey results
establish a high-level baseline of the state of the
practice for EPA JSRM. 

This paper provides an initial analysis that
identifies similarities and differences among the
centers.  The intent is to give a clear picture of what
exists that would allow for improved decision making
for other centers when trying to advance or introduce
\EPAJSRM solutions. There is a lot of headroom for
further analysis of the survey results.

Each of the surveyed sites is unique in terms of
funding structure, geographical as well as geopolitical
situation.  The combination of these factors has a
strong impact on the possibilities and approaches the
sites are willing to take and can take.  Without such a
survey, the agendas and conclusions that can be drawn
stay hidden.

The survey conducted, and the subsequent analysis in
the paper do not advocate a definite way for how HPC
centers should approach energy management from a job
scheduling and resource management standpoint, but
rather gives a comprehensive overview on information
provided by centers actively pursuing this approach in
production sites backed by technology, research efforts
and collaborations. The survey results include
evaluative statements, such as claiming that an EPA
JSRM solution has yielded energy efficiency
improvements.  This paper is reporting on sites'
evaluation and not making claims about results achieved
with EPA JSRM implementations.

With a broad interest in EPA solutions, a lot of
research is conducted and published in this area
whereas, in contrast, publications are rare about
technology that makes it into real production systems.
There may, however, still be opportunities for research
and the results of this survey may be of interest to
the research and academic community. Understanding what
is used and tried in production, and why, is a valuable
point of information, to drive future directions.
Active vendor engagement is apparent in EPA solutions
as well, since energy efficiency is a key concern.
Most HPC centers are concerned with operational costs
and energy efficiency and some even have a broader
focus on sustainability.  A lot of vendors try to
incorporate EPA solutions into at least part of their
current portfolio, all emphasizing different aspects.
With funding agencies trying to keep low operational
expenses, the introduction of limits to power and
energy consumption is sometimes even labeled with a
strict number.  This can be seen by initial exascale
system forecasts, as well as current and upcoming
procurement announcements.  To a lesser degree, this
already started to become apparent in the petascale
procurements (e.g.~\cite{procLRZ2010}).  All these
factors show a heightened interest in EPA solutions in
general, and with \EPAJSRM in particular.
%TODO add cirations to Coral-2 and NERSC8 in addition
%to \cite{procLRZ2010} above.

To provide a context for the motivation of this work,
prior research in electricity grid integration and
demand management should be highlighted.  To better
understand the interaction of electrical grid
integration and supercomputing centers, Bates et
al.~\cite{Bates2015} carried out an analysis of
anticipated usage patterns of supercomputing centers
and how these can be safely integrated into power grid
management for better cost and risk management.  The
paper highlighted possible partnerships and
interactions of Electricity Service Providers (ESPs)
and Supercomputing Centers (SCs).  An extension to the
European area was conducted in a subsequent study by
Patki et al.~\cite{Patki2016}, focused on the
similarities and differences of Electricity Service
Provider-Supercomputing Center (ESP-SC) relationship
based on the geographic locations in Europe and the
United States.  The focus of the cited work is
the interaction of demand management and SC
coordination, showing potentials, but also necessary
regulatory and technological steps.

The focus of this work is on steps and technological
advances and approaches taken by the supercomputing
centers for job scheduling and resource management
systems to monitor, control and steer power and energy
consumption at the SCs.

The remainder of this paper is structured as follows:
Section~\ref{sec:survey_motivation} introduces a detailed insight on
the motivation that drove the
survey. Section~\ref{sec:survey_methodology} gives insight into the
survey methodology, the design of the study, and the center selection
process and criteria.  Section~\ref{sec:resource_mgr} provides a
description of job scheduling and resource management software used in
HPC centers.  Section~\ref{sec:questions} summarizes the questions and
answers posed in the survey.  Section~\ref{sec:analysis} discusses the
results of the survey and gives detailed analysis on solutions
identified at the centers and builds the main contribution of this
paper.  Section~\ref{sec:related} gives a short overview of related
work to make clear how to position this work in the literature.
Finally, Section~\ref{sec:nextsteps} concludes the paper.
