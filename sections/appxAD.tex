\section{Artifact Description}
\label{appx:AD}

``Authors are asked to voluntarily submit an \textit{artifact description (AD)} appendix along with their paper, describing the details of their software environments and computational experiments to the extent that an independent person could replicate their results.'' 
This paper does not describe a software environment and computational experiments.
It does, however, describe an organizational and technical environment and it uses qualitative survey techniques to gather information.
This AD is an attempt to describe the details of the methodology used for collecting and analyzing survey data in order to explore the extent to which an independent person could replicate the results.

\subsection{Background and motivation}
We have already achieved major gains in energy-efficiency for both the datacenter and HPC equipment.
For example, the PUE of the Swiss Supercomputer (CSCS) datacenter prior to 2012 was 1.8, but the current PUE is about 1.25; a factor of \texttildelow1.5 improvement. HPC system improvements have also been very strong, as evidenced by FLOPS/Watt performance on the Green500 List.
While we have seen gains from data center and HPC system efficiency, there are also energy-efficiency gains to be had from software.
The software stack offers many opportunities for energy efficiency.  
There is a lot of research that explores these software opportunities, but little has been deployed in a production environment.

Through polling of experts, energy and power aware job scheduling and resource management (EPA JSRM) was identified as a software capability that was ahead of other software capabilities in that it actually has been/is being deployed on large-scale production supercomputers.  Other capabilities were considered to be interesting, but as research or investigative technologies.

This work set out to identify and interview all of the supercomputer sites that met the following criteria:
\begin{enumerate}
    \item The center should be representative of an HPC center and have at least one system that is (or could be) in the Top500 list of fastest compute systems in the world.
    \item The center should have either actively deployed or are engaged in technology development with the intention to deploy large-scale EPA JSRM technologies in a production environment.
    \item The center’s leadership was willing to participate and openly talk about their efforts in the area.  
\end{enumerate}

The nature of the investigation was meant to capture and document the state of the practice for EPA JSRM.  Who are the sites?  What have they deployed and/or are investigating?  Why are they doing this?  Are there any best practices or lessons learned?

\subsection{Methodology}
\begin{enumerate}
    \item Identification of sites that met the above criteria was achieved by networking with experts in major supercomputing centers in Europe, Japan and the United States.  It was cross-checked in two public forums with 50+ participants at both SC17 and the European HPC Infrastructure Workshop.  System integrators and JSRM vendors were also queried to help identify sites.  This was meant to be a comprehensive list, not a sample of a larger population.  
    \item At least one site was identified that met all the criteria except that could not openly talk about their efforts in this area.  It was not even possible to determine whether this was a single site, or multiple sites.  For ease of description, we considered this as ‘one site.’
    \item Another site was identified (Genci CINES) as meeting the criteria except that they declined the opportunity to participate.
    \item Developing the questionnaire for SCs to learn more about their EPA JSRM capabilities was done with a collaborative team including users, system integrators and JSRM vendors.  We decided to use open-ended questions as opposed to multiple-choice questions.  The reason for this decision was the concern that EPA JSRM capabilities are all unique and multiple-choice questions would be too restrictive.   The questionnaire was sent and completed as a word document.
    \item Overall, eleven sites were identified and nine sites agreed to provide written answers to the questions in the questionnaire and then to participate in an interview.  
    \item After examining responses to the survey questionnaire from each site, a three-person sub-team interviewed personnel from each site to clarify details in their written survey responses or to ask for further technical details of responses that seemed especially noteworthy.
    \item A transcript combining the written and verbal responses was created and reviewed for approval from each of the nine participating sites.  
    \item Analysis of each of the responses was done by at least two independent researchers for each question.  The analysis was collated into a spreadsheet.  The analysis was reviewed with a team of 7+ other researchers.  The researchers included experts in SC operations, EPA JSRM, and energy efficient HPC architecture, and other layers of the software stack.
    \item An early version of the analysis was reviewed in a face-to-face SC17 forum that included participation from all of the interviewed sites.  The same forum included 40+ other participants who were asked to provide feedback.
\end{enumerate}

\subsection{Reproducibility}
The results need to be accurate and reproducible for the sites interviewed, but the research collected qualitative data and produced qualitative analysis.  Therefore, there is not a quantitative error-bar.  The authors recognize that there are sources of potential error and that a high-bar for reproducibility may be challenging.

Below are some sources of error and risks to reproducibility.
\begin{enumerate}
    \item Open-ended questions produce somewhat subjective answers and add some source of error, but the subject matter of the investigation did not allow for multiple-choice questions because contracts are unique.
    \item Almost all of the subject matter under investigation is not standardized (motivations, site descriptions, EPA JSRM capabilities) and so an apples-to-apples comparison and analysis has a subjective element.  
    \item The questionnaires often required completion by multiple people.  This communication was valuable and instructive, but communication and coordination between people adds potential error. 
    \item The questionnaires were written in English and the answers completed in English.  Some of the people who completed the questionnaires were not native-English speakers.  The 6-person research team conducted their research in English, but were also not all native English speakers.  
    \item This research is more of collection of case studies than a statistical analysis.  The target population is small, although the sample size is almost equivalent to the population. It is impossible to estimate a confidence interval, confidence level or standard of deviation.
    \item Conditions change over time; the questionnaire was completed for a particular situation during a specific time duration.  Reproducing the data results would be possible, but difficult.  
    \item The analysis required interpretation of the data.  The responses were not always clear.  The same thing can be called by two different names.  Or, the same name can be used for two different things. 
    \item The sites span the geographic regions of Asia, Europe and the United States. The centers span academic institutions and national research laboratories with different foci. The differences also become apparent in funding strategy and sources, energy billing, energy service provider environment, but also geographic and thus thermal environment.
    \item The answers were first requested in written form and then followed up by a telephone interview to clarify answers and get deeper insight into the responses. This was especially important when sites answered widely different, omitted responses or were going into extensive detail. The total number of pages for responses ranged from 8 to 17 pages per center.
\end{enumerate}

\subsection{Survey Questions}
\label{appx:Q}
The following is the listing of the questions.

\begin{enumerate}[{Q}1:]
\item What motivated your site's development and implementation of energy
    or power aware job scheduling or resource management capabilities?

\item Please describe your data center and major
    high-performance computing system or systems where energy or power aware
    job scheduling and resource management capabilities have been deployed in a
    way that covers some or all of the following points of interest.

    \begin{enumerate}[(a)]
        \item Total site power budget or capacity in watts.
        \item Total site cooling capacity.
        \item Major high-performance computing system or systems in terms related to:
            number of cabinets, nodes, and cores;
            peak performance;
            node architecture, high-speed network type, memory;
            peak, average, and idle power draw.
    \end{enumerate}


Other information to help describe site/system level drivers for energy or
power aware job scheduling and resource management.

\item Describe the general workload on your high-performance
    computing system or systems.
    Specifically, any or all of the following would be useful:
    \begin{enumerate}[(a)]
        \item What is running right now, or what does a typical snapshot look like?
            How many jobs are running?
            What sizes are these jobs?
            Generally how long do jobs run?
        \item What does the backlog of queued jobs look like?
            How many jobs are currently waiting?
            What are the sizes of waiting jobs?
            How long will they run?
        \item What is the throughput of your system?
            Approximately how many jobs per month?
        \item In simple terms, describe your main scheduling goal.
            Possible examples of scheduling goals might include priority, turn-around
            time, fairness, efficiency, or system utilization.
            What percentage of your system’s use would you consider to be “capability”
            (using the maximum computing power to solve a single large problem in the
            shortest amount of time) or “capacity” (using efficient cost-effective
            computing power to solve a few somewhat large problems or many small
            problems)?
        \item If you have statistical information available, what is the minimum,
            median, maximum, and 10th, 25th, 75th, and 90th percentile job size
            and wallclock time?
    \end{enumerate}
\item Describe the energy and power aware job scheduling and
    resource management capabilities of your large-scale high-performance
    computing system or systems.

\item List and briefly describe all of the elements that
    comprise your energy and power aware job scheduling and resource management
    capabilities.
    \begin{enumerate}[(a)]
        \item Include an implementation time component to your answer (this is, when
            was it implemented?).
        \item Are these elements commercially available supported products?
        \item Has there been much non-portable/non-product work done to implement
            your capabilities?
    \end{enumerate}

\item Do you have application/task level joint optimization,
    such as topology-aware task allocation, as a way of directly improving
    energy consumption or indirectly improving energy consumption (for example,
    by improving application performance, resulting in reduced wallclock
    time)?
    Did you engage software development communities to improve your energy and
    power aware job scheduling and resource management solution for this
    capability?

\item How well does your solution work?
    What are the advantages and disadvantages of your implementation?
    Describe any results, benefits, or unintended consequences of your
    implementation.

\item What are the next steps for the energy or power aware job
    scheduling and resource management capability you have developed?
    \begin{enumerate}[(a)]
        \item Do you intend to continue site development and/or product deployment?
        \item Will your planned next steps drive new requirements in procurement
            documents, NRE funding, etc.?
    \end{enumerate}

\end{enumerate}
