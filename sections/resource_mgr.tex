Throughout this paper we refer to two types of system software that we
specifically define here.  Job schedulers allow high-performance
computing users to efficiently share the computing resources that
comprise an HPC system.  Users submit batch jobs into one or more
batch queues that are defined within the job scheduler.  The job
scheduler examines the overall set of pending work waiting to run on
the computer and makes decisions about which jobs to place next onto
the computational nodes within the computer.  Generally speaking, the
job scheduler attempts to optimize some characteristic such as overall
system utilization or fast access to resources for some subset of
batch jobs within the computing center's overall workload.  The
various queues that are defined within the job scheduler may be
designated as having higher or lower priorities and may be restricted
to some subset of the center's users, thus allowing the job scheduler
to understand distinctions of importance of certain jobs within the
overall workflow.

To carry out its work, a job scheduler typically interacts with one or
more resource managers.  A resource manager is a piece of system
software that has privileged ability to control various resources
within a datacenter.  These resources can include things such as the
physical nodes that make up a high-performance computer's
computational resources; disks, disk channels, or burst buffer
hardware that comprise I/O resources; or network interfaces, network
channels, or switches that comprise interconnect resources.  For
example, a job scheduler might use resource management software to
configure the processing cores, memory, disk, and networking resources
within one or more computational nodes in accordance with the
requested resources for a specific batch job prior to launching that
job onto the allocated computational nodes.  Finally, in some cases,
resource management software might have the ability to actuate pieces
of the physical plant that are responsible for delivering electricity
to the datacenter or cooling the datacenter.

The job scheduler can affect the energy and power characteristics of a
high-performance computer in a number of ways.  For example, since the
job scheduler understands how many resources of each type are required
to run a given job, it can use this knowledge as a basis for
estimating the power requirements for the job.  Further, since the job
scheduler also knows the other jobs that are currently running on the
HPC, it can also estimate the total power draw of the entire computer.
In circumstance where there is a power limit for the entire
datacenter, as is the case for some sites that participated in our
survey, the job scheduler can use this knowledge to ensure that the
supercomputer does not exceed this limit.  Along those same lines, if
the job scheduler maintains some historical database of each job's
resource usage, the job scheduler can likely make even more informed
resource management decisions.  At a straightforward level, the job
scheduler can use historical knowledge about the job to make more accurate
power estimates.  At a more complex level, the job scheduler may be
able to use historical knowledge about the job to determine trade-offs
regarding performance versus power consumption, particularly in cases
where a given job has been run in the past using various numbers of
resources.  This more complex level of analytics can be used to drive
decisions about the energy consumption of a job, such as whether
allocating more computational resources to the job causes more or less
consumption of energy.  Overall, notions such as these form the basis for
energy and power aware job scheduling and resource management considered
in this paper.

\begin{figure*}[ht]
\centering
\includegraphics[width=0.75\textwidth]{images/sc18_epajsrm_scope.jpg}
\caption{Interactions among multiple components that make up a typical
\EPAJSRM (Energy and Power Aware Job Scheduling and Resource Management)
solution.}
\label{fig:EPA_JSRM_solutions}
\end{figure*}

Figure~\ref{fig:EPA_JSRM_solutions} presents an overview
of the different components that may participate in such a solution.
As depicted in the figure, an \EPAJSRM solution can be thought of as
introducing two broad categories of functionality.  First, monitoring
infrastructure must augment the capabilities traditionally found in
job schedulers and resource managers for monitoring the utilization of
nodes, disks, and network devices to also include new functionality
for monitoring power telemetry of these devices along with,
potentially, devices associated with the datacenter physical plant
(e.g., temperature sensors, humidity sensors, chillers, etc.).
Second, control functionality must allow actuating these devices in
ways that expand upon traditional control mechanisms.  Control
techniques might range in complexity from simple human-controlled
actuation of processor dynamic voltage and frequency settings to
reduce power to much more complex scenarios where the job scheduler
has detailed historical knowledge of job characteristics and schedules
multiple jobs simultaneously in a way that optimizes for certain
energy-specific or power-specific objectives.  Because system-wide
software agents like the job scheduler have access to details of a
supercomputing center's entire workload, and can potentially apply
advanced data analytics to the problem, they have the potential for
improving the energy and power consumption of supercomputers in ways
that are unlikely to be possible for human-controlled processes.
Accordingly, we expect that a trend in coming years will be to have
system-wide techniques play an increasing role in these endeavors.
Due to the previous absence of a general overview of EPA solutions
employed, actual usage and benefits were only known within individual
centers.
