This section of the paper presents a high-level analysis of the survey
responses presented in Section~\ref{sec:questions}.  The goal of this
analysis is to identify commonalities among approaches taken by
several supercomputing centers as well as to identify particularly
noteworthy approaches.  The responses represent the efforts by
leading-edge supercomputing centers to develop energy and power aware
job scheduling and resource management solutions.  To that end, we
believe that this analysis will be of interest to the supercomputing
community.

%% \subsection{Analysis of Motivations}
%% \label{sec:analysis_motivation}
%% \input{sections/analysis/analysis_motivation}

%% \subsection{Analysis of Adopted \EPAJSRM Approaches}
%% \label{sec:analysis_techniques}
%% \input{sections/analysis/analysis_techniques}

\begin{table*}[h]
  \begin{center}
    \caption{Total power and cooling budget with maximum, average and idle power draws (units are in MW)}
    \label{tab:pwr_consumption}
    \begin{tabular}{|c||c|c||c|c|c|}
      \hline
      \textbf{Center} & \textbf{Power Budget} & \textbf{Cooling Budget} & \textbf{Maximum draw} & \textbf{Average draw} & \textbf{Idle draw} \\
      \hline
      \hline
      \textbf{RIKEN} & 23 & 36 & 15 & 12 & 10 \\
      \textbf{TokyoTech} & 2 & 2 & 1.4 & 0.8 & 0.55 \\
      \textbf{CEA} & 10 & 7.5 & & 5 &  \\	
      \textbf{KAUST} & 3.6 & 2.9 & 3 & 2 & 0.55 \\
      \textbf{LRZ 1} & 10 & 10 & 2.9 & 2.2 & 0.7 \\
      \textbf{LRZ 2} & 10 & 10 & 1.5 & 1.2 & 0.4 \\
      \textbf{STFC} & 4.5 & 2 & 1 & & \\		
      \textbf{Trinity} & 19.2 &	27 & 8.4 & & 2.4 \\
      \textbf{Cineca} &	6 & & & & \\
      \textbf{JCAHPC} &	8 & 4.2 & 3.2 & 2.4 & \\	
      \hline
    \end{tabular}
  \end{center}
  \vspace{-1mm}
\end{table*}

\begin{figure}[h]
  \begin{center}
    \includegraphics[width=0.85\linewidth]{images/pwr_consumption.pdf}
    \caption{Maximum, average and idle power consumption as percentage of total power budget}
    \label{fig:pwr_consumption}
  \end{center}
\end{figure}

Table~\ref{tab:pwr_consumption} summarizes direct responses from the
sites regarding power and cooling capacity as well as maximum, average,
and idle power draws for each site.  Following from the data presented
in the Table, Figure~\ref{fig:pwr_consumption} presents a graph of
the percentage of total power capacity that is reached for each of
maximum, average, and idle power draw at each site.

Broadly speaking, energy and power aware job scheduling and resource
management techniques used by sites that participated in the survey
can be divided into two approaches.  First, several sites primarily
focused on approaches related to powering down idle resources or
related to quickly damping computing resources to make them idle or
nearly idle, typically to reach some prescribed power threshold within
a short amount of time (seconds or minutes).  Second, some sites
described approaches related to managing active resources in a way
that improves their consumption of energy or power while still being
engaged on computational work.  The following subsections explore
these two broad categories in greater detail.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Managing Idle Resources}
\label{sec:Analysis:Idle}

Energy and power aware job scheduling and resource management
techniques that focus on managing idle resources are straightforward
approaches that can be especially effective for specific use cases.
These strategies tend to skew toward short-term objectives such as
keeping the overall power draw of a supercomputer under a predefined
specific threshold.

In the simplest approach, the batch job scheduler can examine the
state of computational resources to determine the set of resources
that are idle and can combine this information with knowledge of
upcoming jobs.  In cases where there are idle resources and no
upcoming jobs exist to fill those resources, the scheduler can
instruct the resource management software to
\textit{shut down the idle resources}.  There are two advantages
of this approach.  First, the powered off state is, by definition, the
state in which the computer is consuming the least amount of
electricity.  Second, the approach can be implemented very simply with
no modifications required to application software.  The only system
software support necessary is within the job scheduler and resource
manager, and job schedulers such as Moab~\cite{Moab,moab_greenComputing}
and SLURM~\cite{Slurm,Slurm_powerSaving_guide} have supported this
functionality for several years.  The disadvantage of this approach is
that supercomputers typically have consistently high utilization
except during scheduled maintenance windows. Due to the high purchase
costs and limited lifespans of supercomputers, typically around 60
months, the sites that operate them must maintain high utilization
which means few times when resources are idle.

One site that uses the technique of shutting down idle resources is
Tokyo Institute of Technology.  The benefits of the approach can be
observed in Figure~\ref{fig:average_pwr} which shows the percentage
of average power draw to maximum power draw for each center.  Tokyo
Institute of Technology can be observed as having the most favorable
relationship of average power draw to maximum power draw, just above
50\%, due to the aggressively shutting down idle nodes.

\begin{figure}
  \begin{center}
    \includegraphics[width=0.85\linewidth]{images/average_pwr.pdf}
    \caption{Average power consumption as percentage of maximum power consumption}
    \label{fig:average_pwr}
  \end{center}
\end{figure}

Another approach is \textit{dynamic job termination}.  Dynamic job
termination is a brute-force method of responding to power constraint
situations by terminating one or more running jobs in order to keep
the total system power draw under some predefined critical limit.
After terminating jobs, the job scheduler may further reduce power
consumption by shutting down now-idle resources as described above.
The task of determining which jobs to terminate may be done manually
by a human based on knowledge of the running workload or may be
automated via software that interacts with the job scheduler.  Such
software might select jobs to terminate by size (i.e., terminate the
largest job until system power falls below the critical threshold), by
running time (i.e., terminate the job that has been running for the
least amount of time using the notion that this job has the least work
that might be lost), or by active power telemetry that allows the job
scheduler to estimate the power draw of each job and find a ``best
fit'' of jobs to kill to reach the critical threshold.  In cases
where a given job has the ability to checkpoint its work prior to
being terminated, the job scheduler may take appropriate measures to
properly signal the job accordingly prior to terminating the job,
subject to the time requirements for writing a checkpoint file versus
the time frame within which the system power draw must be brought under
the critical threshold.

Sites that employ dynamic job termination include RIKEN.  At RIKEN,
when the resource management system detects an excessive power usage,
it automatically stops jobs by canceling (not suspending) the job
using the largest number of nodes, although research is currently
being conducted into more sophisticated policies.

An alternative to outright terminating jobs in response
to a violation of a predefined critical system power
threshold, is to trigger the hardware to enter into a
low power states like P-states (P0--Pn) and C-states
(C0--Cn). These
states - as defined by the ACPI (Advanced Configuration and
Power Interface) specification\cite{ACPIspec} - 
map to different modes of operation of the
components. 

P-states correspond to discrete performance states of
hardware that map to different voltage/frequency
levels. The ability to control these states on-demand
is called - Dynamic Voltage Frequency Scaling or DVFS.
Using DVFS, voltage and frequency can be scaled down,
resulting in a corresponding decrease in both power
draw and the associated performance of the machine. For
systems that expose these control knobs to the software
stack, it may be possible to bring the overall power
draw of the machine under a specified threshold,
without resorting to outright termination of jobs. 

Relative to P-states, C-states allow hardware
components to enter into modes that are much
lower-powered than P-states. Unlike a P-state where a
hardware component is still operational, all C-states
(except C0) correspond to hardware states where
multiple subcomponents are completely turned off to
achieve power-savings.  Therefore, triggering hardware
components into low-powered C-states is a preferred
approach for idle node power management. It must be
noted, however, that switching between C-states comes
at a cost: The transition latency between a low-powered
non-operational C-state (C1--Cn) into a
fully-operational state (C0) is significantly higher
than that between two P-states. As a result, JSRM
solutions that use C-states to limit idle-node power
have to tolerate the penalty while switching the nodes
to `low-powered idle' to `full-active' states during
job allocations.

No site reported using these hardware states with
the explicit intention of tackling the violation of the
system power threshold. That said, some sites
like CEA, LRZ, and STFC reported using resource
managers like Slurm and IBM Load Leveler that leverage
these hardware states to limit the power
consumption by idle and active nodes.
Section~\ref{sec:Analysis:Active} below, describes the
use of P-states (DVFS) as part of an overall strategy
to reduce energy consumption of \textit{active} nodes
over the longterm operation of a supercomputer.

The final approach discussed in this section is
\textit{static or dynamic resizing of job resources}.  In this
approach, the total number of compute nodes allocated to a job is
limited, either statically when the job is released from the batch
queue and launched onto resources or dynamically while the job is
running.  By reducing the number of resources allocated to a job, the
resources immediately draw much less power than if they were actively
engaged in executing a job.  Additionally, nodes that become
unoccupied due to dynamically migrating work away could be candidates
for shutdown as described previously.  This approach involves some
knowledge of the workload to be effective.  For example, for the case
of a traditional supercomputing workload consisting of tightly coupled
scientific applications that are submitted into a batch system with a
specific number of requested processing elements, it may not make
sense to the problem's semantics for the batch system to allocate a
different number of processing elements when the job is launched.
These cases would be if the job requests and requires a specific
number of processors, such as a power of two number of processors or a
square or cube number of processors, in order to properly distribute
the discrete pieces of the problem.  Advanced runtime systems such as
Charm++ \cite{kale2012charm} that decouple the logical decomposition of a job from
its physical resources could accommodate such static launch-time
changes in the number of allocated resources as well as allowing
dynamic run-time changes.  Furthermore, some centers that participated
in the survey reported having some fraction of their total work-flow
consumed by cloud-type jobs, and these are ideal candidates for
dynamic resizing of job resources due to the elastic nature of these
jobs.  For example, MapReduce jobs can be readily reduced in size by
having the mapper reduce the number of candidate nodes onto which
tasks are placed. That is to say, the inherent characteristics of
the MapReduce programming model that make it fault tolerant can also
be leveraged to allow it to accommodate the power-related mission of
a supercomputing center.

While the strategies for energy and power aware job scheduling and
resource management that deal with characteristics of idle nodes are
typically straightforward to implement, their impact tends to be
limited to a supercomputing center's short-term objectives.
Approaches that have a more broad impact related to long-term energy
reduction are discussed in the next section.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Managing Active Resources}
\label{sec:Analysis:Active}

The second broad category of energy and power aware job scheduling and
resource management techniques focuses on managing active resources.
Supercomputers tend to be quite expensive and, accordingly, sites that
operate large supercomputers have an incentive to operate the machines
in a state of high utilization where computational resources are
nearly always active.  Typically, managing these active resources
requires striking a balance among several constraints such as job
performance, system utilization, and energy consumption.  Striking
such a balance often requires taking a longer-term perspective in
which techniques are applied over a substantial fraction of a center's
workload and have an impact to a center's overall energy profile
rather than to a point-in-time power consumption objective.

%% Matching power supply to power demand
%% GAK: Somebody put this in, but I don't know what it means.

One approach to managing active resources is
\textit{application tagging}.  With this approach, users inform the
job scheduling and resource management system about characteristics
of submitted jobs by manually applying one or more ``tag values'' to
each job.  The job scheduling system can examine these tag values to
adjust system configuration parameters accordingly.  For example, a
job that is heavily I/O bound could be tagged as such by the user and
the job scheduling system might launch the job onto nodes that have
been adjusted to have a more energy efficient P-state using Dynamic
Voltage and Frequency Scaling.  In cases of a I/O bound jobs, this
P-state adjustment is unlikely to have a significant effect on job
performance but could potentially significantly reduce the energy
consumed by I/O bound jobs over a long period of time.  A challenge
with this approach is that it depends on each user's ability to
identify the important characteristics of each job to the job
scheduler by applying appropriate tag values.

LRZ is an example of a site that applies application tagging to great
effect. LRZ uses the IBM LoadLeveler scheduler~\cite{loadLeveler}
and encourages users to tag their applications with metadata that
allows the scheduler to choose ideal DVFS settings for the nodes
allocated to jobs.  The results of this can be observed in
Figure~\ref{fig:pwr_consumption} where LRZ's maximum and average
power draw, taken as a percentage of total power capacity, are
very similar.  The long-term effect of the use of application
tagging at LRZ is an overall reduction in the amount of energy
used.
The LoadLeveler's capability for tagging and thus energy aware
scheduling was developed in a project collaboration of IBM with
LRZ. This was a specific requirement for the SuperMUC system.
This work involved development of models for the system as well
as understanding energy response of DVFS to the LRZ application pool.
The concepts are explained in the work by 
Auweter~et~al.~\cite{10.1007/978-3-319-07518-1_25} and are done  
using an initial run of the application with a (low) default
frequency and then setting the frequency for subsequent submission 
of the same application at higher frequencies according to the model 
if energy/performance trade-off is favorable.
Similar open-source technology is planned to be integrated in SLURM
for upcoming machines at LRZ. %Is this OK? This knowledge can be derived from the procurement document (Semi official in there.)

An extension of the application tagging approach is the use of
\textit{predictive models} within the job scheduling and resource
management system.  With this approach, the job scheduling system
maintains a database of all job submissions for each user including
characteristics such as job size (number of nodes or processing
elements), total execution time, executable pathname, and
input arguments.  In addition to this information, telemetry
measurements such as node power measurements for the computing
resources used by each job are recorded in the database.  Through
regression analysis at the completion of each job, the job scheduling
system can attempt to identify characteristics for each job, such as
node DVFS settings, with which each job runs most energy efficiently.
Upon subsequent submissions of a specific job, the optimal
configuration can be applied automatically to the resources allocated
to the job.  One challenge to this approach involves capturing and
maintaining historical data for each batch job along with telemetry
data for all computational resources.  A possibly bigger challenge,
however, involves automatically recognizing the combination of
executable, input arguments, and data files that constitutes a
distinct instance of a job.  Consider cases where a user submits one
job with a given executable and a second job with a different
executable.  Assuming that these jobs would be completely different
instances seems straightforward.  However, in cases where a user
submits two jobs with the same executable but with different input
datasets, the two jobs might perform similarly or might perform
quite differently.  For example, the two jobs might use the same
executable such as a molecular dynamics solver but the first input
dataset might represent a very simple molecule to simulate while
the second input dataset might represent a very complex model.
At least one site, LRZ, reported that they were working toward
objectives along this path.

The final approach considered in this section is
\textit{topology-aware job placement}.  This approach takes into
consideration the communication patterns employed by a parallel application
and attempts to place the processing elements of the job onto
resources in such a way as to optimize communication.  In order for
this placement to happen, the parallel runtime system layer must be
instrumented to capture details of each message sent between each pair
of processing elements in the job.  Then, in a subsequent off-line
process after the job completes, this message trace is used to
partition the communication graph either in terms of the number of
messages passed between PEs or in terms of the total amount of message
traffic passed between PEs.  The next time the same job type is
submitted by the user (as above, determined either by requiring the
user to identify the job using some kind of self-applied tagging
mechanism or by the system attempting to automatically identify that a
new job is likely a similar instance to a previously seen job) the job
scheduler allocates resources and uses the resource manager to assign
PEs to the resources in such a way as to optimize the communication
graph.  For example, two PEs that communicate frequently would be
placed onto nodes that are physically near each other
on the network topology.  This approach
works best for tightly-coupled parallel applications that are somewhat
asymmetrical (e.g., unstructured mesh decomposition or
particles-in-boxes decomposition) because this characteristic provides
good places for partitioning the communication graph.  When
successful, this approach can improve the energy consumption of a job
directly and indirectly.  Energy consumption improves directly because
less network hardware is energized in running the job.  Energy
consumption improves indirectly due to improving performance of the
job related to communication performance.  From the survey results,
CEA reported that they use a form of topology-aware job
placement as supported by SLURM.

%% Possibly talk about vendor support here.  The idea is that the
%% approaches described in this subsection are more sophisticated
%% and end up relying a lot more on vendor support to be successful.

%\input{sections/analysis/analysis_prelude}
%\input{sections/analysis/analysis_milos}

% FIXME: Decide what parts (if any) should go in?
%\input{sections/analysis/analysis_milos2}
