%Find different headings:
%\subsection{Academic Research}
%% Several research avenues have been explored in order to curtail the power 
%% and energy consumption of HPC systems. In the rest of this section we are 
%% going to present a brief overview of the techniques found in the 
%% literature and current state-of-the-art.

Multiple surveys of research on power management techniques for high
performance computing systems have been previously published~\cite{maiterth2017power, Liu2010, cameron2005high}.
As clearly pointed out by Hsu et al.\cite{HsuFA05}, a change in
perspective is required:
the focus must shift from performance-based metrics (such as the
performance-power ratio) to new metrics which take into account different
aspects of the problem, such as integrating notions of total cost of
ownership, productivity, and reliability.

Several works aim to reduce the overall power consumption at the supercomputer level.
Sarood et al.~\cite{Sarood2014} describe an integer linear programming model to enforce power capping in an HPC cluster through over-provisioning. Their approach combines over-provisioning with a power-aware scheduler.
Mammela et al.~\cite{mammela2012energy} present an energy-aware scheduler that turns off idle nodes when the scheduler detects that no activity can be scheduled for a sufficiently long time on a given node.

Many approaches take advantage of ``moldable jobs'' that can run with different configurations (e.g., number of nodes, cores, or threads)~\cite{Bailey:2014:ACS:2703009.2706736,Patki:2015:PRM:2749246.2749262,Mualem:2001:UPW:380314.380315}.  Given a desired power consumption budget, the best configuration is selected for each job before it is started.  Other authors have exploited the power and performance variability among nodes and components within the same system \cite{Inadomi:2015:AMI:2807591.2807638,shoukourian2015adviser}.

Dynamic Voltage and Frequency Scaling (DVFS) allows processor performance to be exchanged for reduced power consumption and has been explored as a means to increase a supercomputer's energy-efficiency~\cite{8081827, 10.1007/978-3-319-07518-1_25,Freeh:2007:AET:1263127.1263246}. However, reducing the operating clock may increase an application's runtime and negate the benefits of this technique~\cite{8081827, 10.1007/978-3-319-07518-1_25, Hsu:2005:PRS:1105760.1105766}. Approaches for overcoming this issue take advantage of compute, memory, and communication phases~\cite{Freeh:2007:AET:1263127.1263246} as well as by exploiting heterogeneous nodes \cite{8081827}.
An extension of these approaches to job scheduling is discussed by Etinski et al.~\cite{Etinski2010,Etinski:2012:PJS:2400744.2400966}, which extends the standard job scheduling algorithm with a power budgeting capability through DVFS.

An alternative to the direct control of frequency scaling is Intel's Running Average Power Limit (RAPL)~\cite{David:2010:RMP:1840845.1840883}, which provides a software configurable and hardware enforced power cap. Several works have combined Intel's RAPL feature with job scheduling algorithms~\cite{Bodas:2014:SPS:2689710.2689713,Ellsworth:2015:DPS:2807591.2807643}. These approaches rely on allocating a reduced power budget to each node or socket. Ellsworth et al.~\cite{Ellsworth:2015:DPS:2807591.2807643} dynamically shift the available power budget between nodes to give more power to nodes that are on an application's critical path.

An orthogonal approach for achieving a system level power budget is controlling the number of concurrently running jobs~\cite{BorghesiHPCS15, BorghesiCP15, BorghesiISC16}.

Other work focuses on minimizing energy consumption and/or related energy costs~\cite{10.1007/978-3-319-07518-1_25,BHATTACHARYA2013183,KHEMKA201514,Leal2016}. These approaches act on the job execution order alone, without requiring any hardware modifications or change in the operating frequencies of compute nodes.

A priori information about an application's power usage characteristics can be useful for making EPA-informed decisions prior to scheduling a job.
This information may include observations of an application's past behavior, historical data and model regression~\cite{shoukourian2014predicting}, user-provided information such as a tag identifying similar jobs\cite{10.1007/978-3-319-07518-1_25}, machine learning techniques and job submission information~\cite{BorghesiISC16,Sirbu:2016:PCM:2990973.2991005}.

%From this discussion, we can see that in recent years several techniques, algorithms, and strategies have been proposed in the state-of-the-art for energy and power aware job schedulers and resource managers in the high performance computing domain.
The findings of the survey described in this paper show early attempts at deploying EPA JSRM technologies on large scale production deployments in supercomputing centers. This contribution highlights the large gap between current EPA JSRM practice and related research in the literature, providing guidance for both future research and production-oriented activities.
