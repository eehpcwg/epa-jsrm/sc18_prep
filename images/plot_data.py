#!/usr/bin/python

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

font = {'size'   : 14}
matplotlib.rc('font', **font)

xaxis_labels = []
legend_1 = ("Maximum draw", "Average draw", "Idle draw")
legend_2 = ("Average draw")


data = [
    [], # maximum
    [], # average
    [], # idle
    [], # percentage
]

first_graph_bars = 3
second_graph_bars = 1

with open('data.csv') as fdata:
    lines = fdata.readlines()
    # skip header
    for line in lines[1:]:
        entries = line.split(',')
        
        # the first column is legend
        xaxis_labels.append(entries[0].strip())

        for k in range(0, len(data)):
            data[k].append(float(entries[k+1]))
        # average draw
        # idel draw

ind = np.arange(len(xaxis_labels)) # the x locations for the groups
width = 0.25 # the width of the bars


fig, ax = plt.subplots()
rects1 = ax.bar(ind, data[0], width, color='b')
rects2 = ax.bar(ind + width, data[1], width, color='r')
rects3 = ax.bar(ind + 2*width, data[2], width, color='g')

ax.set_ylabel('Percentage of total site budget (%)')
ax.set_xticks(ind + 1.5*width)
ax.set_xticklabels(tuple(xaxis_labels), rotation = 'vertical')
ax.set_xlim(0, ind[-1]+3*width)
ax.set_ylim(0, 90)

ax.legend((rects1[0], rects2[0], rects3[0]), legend_1)

plt.tight_layout()
fig.savefig('pwr_consumption.pdf')
plt.close()

fig, ax = plt.subplots()
width = 0.8
rects1 = ax.bar(ind, data[3], width, color='b')

ax.set_ylabel('Percentage of maximum power draw (%)')
ax.set_xticks(ind+0.5*width)
ax.set_xticklabels(tuple(xaxis_labels), rotation = 'vertical')
ax.set_xlim(0, ind[-1]+width)
ax.set_ylim(0, 90)

#ax.legend((rects1[0]), legend_2)

plt.tight_layout()
fig.savefig('average_pwr.pdf')
plt.close()


    



